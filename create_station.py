from stations.models import Station, Variable, Data
import datetime
import numpy as np
import procweatherdata as prwd
import matplotlib.pyplot as plt
import datetime as datetime
#import subpltset as sps

# Get rid of previous plots, and make plotting interactive (ipython)
plt.close('all')
plt.ion()
#plt.hold('True')

# Define some basics about the station data
station_prefix = 'wyj'
startyear = 1992
endyear = 2013

# Load the data and print out the headers
dataf = prwd.loadstn( '/Users/neil/vicweather/wyj/', station_prefix + '_daily_' , startyear , endyear )
#"""
s = Station(name="VICTORIA UNIVERSITY CS", tcid="WYJ", wmoid=71783)
s.save()
v_tmean = Variable(name='tmean', unit='^oC')
v_tmean.save()
v_tmax = Variable(name='tmax', unit='^oC')
v_tmax.save()
v_tmin = Variable(name='tmin', unit='^oC')
v_tmin.save()

"""
s = Station.objects.get(tcid='WYJ')
v_tmean = Variable.objects.get(name="tmean")
v_tmax = Variable.objects.get(name="tmax")
v_tmin = Variable.objects.get(name="tmin")
"""
#s.variable_set.create(name='tmean', unit='^oC')
#s = Station.objects.get(id=1)
#v = Variable.objects.get(name="tmean")
#e = Data(station=s,variable=v,time=datetime.datetime(2001,1,1),value=2)
#e.save(force_insert=True)

mean_temp = dataf.Mean_Temp.dropna()
min_temp = dataf.Min_Temp.dropna()
max_temp = dataf.Max_Temp.dropna()
tmean = []
tmax = []
tmin = []
for k, mt in enumerate( mean_temp ):
  tmean.append( Data(station=s, variable=v_tmean, time=mean_temp.index[k], value=mt) ) 
for k, mt in enumerate( max_temp ):
  tmax.append( Data(station=s, variable=v_tmax, time=max_temp.index[k], value=mt) ) 
for k, mt in enumerate( min_temp ):
  tmin.append( Data(station=s, variable=v_tmin, time=min_temp.index[k], value=mt) ) 

Data.objects.bulk_create( tmean )
Data.objects.bulk_create( tmax )
Data.objects.bulk_create( tmin )

  #e2 = s.data_set.create(variable=v,time=datetime.datetime(1900+i,1,1),value=20 + i*0.01)

s.data_set.filter(variable__name="tmean")

qs = Data.objects.filter(variable__name="tmin", station__tcid='WYJ')
tmt = [ ob.value for ob in qs ]
tmtime = [ ob.time for ob in qs ]

plt.plot( tmtime, tmt )



