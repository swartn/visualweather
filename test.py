from stations.models import Station, Daily_data
import datetime
import numpy as np
import matplotlib.pyplot as plt
plt.close('all')
plt.ion()

stn = Station.objects.get_or_create(stn_name='yyj')

s = Station.objects.get(pk=1)
print s.stn_name

s.daily_data_set.all()

time0 = datetime.datetime(1992,10,01)
#mtm = np.random.randint( 15 , 25 , 100 )
mtm = np.linspace(0,100,100)*2

for i in range(100):
    mtime = time0 + datetime.timedelta(i) ;
    s.daily_data_set.get_or_create(time=mtime,tmean=mtm[i])

md = [data.time for data in s.daily_data_set.all()]
tmean = [data.tmean for data in s.daily_data_set.all()]
#print 'tmean: ', tmean

plt.plot( md , tmean )


