from django.shortcuts import render, get_object_or_404
from stations.models import Station, Variable, DailyMean, MonthlyMean, AnnualMean, MonthlyClim, DailyClim
from django.http import HttpResponse,HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views import generic
import numpy as np

import matplotlib.pyplot as plt
import pandas as pd
from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
from matplotlib.figure import Figure
from matplotlib.dates import DateFormatter

# Create your views here.
class IndexView(generic.ListView):
    template_name = 'stations/index.html'
    context_object_name = 'stations_list'

    def get_queryset(self):
        """Return list of stations"""
        return Station.objects.all()

class DetailView(generic.DetailView):
    model = Station
    template_name = 'stations/detail.html'

    def get(self, request, *args, **kwargs):
        self.object = self.get_object(queryset=Variable.objects.all() )
        return super(DetailView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(DetailView, self).get_context_data(**kwargs)
        context['variable'] = self.object
        context['variable_year_list'] = [2000, 2001]
        return context

class ResultsView(generic.DetailView):
    model = Station
    template_name = 'stations/results.html'

    def get_context_data(self, *args, **kwargs):
        # Call the base implementation first to get a context
        context = super(ResultsView, self).get_context_data(**kwargs)
        # Add in a QuerySet of the selected variable
        vs = Variable.objects.all()
        context['selected_variable'] = vs
        return context
   
def resultsView2(request, pk, variable_id, selected_year, selected_month, av_per):
    station = get_object_or_404(Station, pk=pk)
    variable = get_object_or_404(Variable, pk=variable_id)
    qs = variable.monthlymean_set.all()
    year_list = list( set([ ob.time.year for ob in qs ]) )
    year_list = year_list[::-1]
    month_list = range(1,13)
    #selected_month = 1
    #cs_month = 1
    ap_list = ['day', 'month', 'year']
    return render(request, 'stations/results.html', {'station': station, 'variable':variable, 'variable_year_list': year_list\
                                                      ,'cs_year':int(selected_year), 'variable_month_list': month_list,\
                                                      'cs_month':int(selected_month)\
                                                      , 'cs_var': int(variable_id), 'cs_ap':av_per,\
                                                      'ap_list':ap_list })

def stationRedirect(request, pk):
        s = get_object_or_404(Station, pk=pk)
        v_id = 1
        v = s.variable_set.get(pk=v_id)
        qs = v.monthlymean_set.all()
        year_list = list( set([ ob.time.year for ob in qs ]) )
        year_list = year_list[::-1]
        y = year_list[1]
        m = 1
        
        return HttpResponseRedirect(reverse('stations:results', args=(s.id, v_id, y, 1, 'month')))

def vote(request, station_id):
    s = get_object_or_404(Station, pk=station_id)

    try:
        y =  request.POST['selected_year']
        m =  request.POST['selected_month']
        v = s.variable_set.get(pk=request.POST['variable'])
        per =  request.POST['averaging_period']

    except (KeyError, Variable.DoesNotExist):
        # Redisplay the poll voting form.
        return render(request, 'stations/detail.html', {
            'station': s,
            'error_message': "You didn't select a choice." ,
        })
    else:
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        print 'hello'
        return HttpResponseRedirect(reverse('stations:results', args=(s.id, v.id, y,m, per)))
    
def pltmean(x, ymean, line=False ):
     """ Black bar plots for the mean """
     if line == False:
         plt.bar( x , ymean , facecolor='black',\
         align = 'center',edgecolor='white')
         plt.ylim([ 0 , np.max(ymean) * 1.50 ] )
     else:
         plt.plot( x , ymean , 'k', linewidth=3)
         plt.ylim([ 0 , np.max(ymean) * 1.50 ] )
         
def pltanom( x , y , ymean , ymax , ymin , ystd):
    """ Anomaly plots, includingn a grey envelope for the std-dev (ystd), colored bars
    for the anomalies (y - ymean) and dashed colored lines for the extremes (ymin/max).
    """
    try:
        xe = np.linspace( min(x) -1 , max(x) +1 , len(x) )
        plt.fill_between( xe , -1.0 * ystd ,\
        ystd , facecolor = 'black', alpha = 0.15)
        plt.plot( xe , np.zeros(len(xe)) , 'k-')
    
        yanom = y - ymean
        neg = yanom < 0.
        pos = yanom >= 0.
        print yanom
    
        plt.bar( x[ neg ], yanom[ neg ], facecolor='red', edgecolor='white', align = 'center')

        plt.bar( x[ neg ], yanom[ neg ], facecolor='#9999ff', edgecolor='white',\
        align = 'center')
        plt.bar( x[ pos ] , yanom[ pos ], facecolor='#ff9999', edgecolor='white'\
        , align = 'center')

        plt.plot( x , ymax - ymean , 'r--' , linewidth=2)
        plt.plot( x , ymin - ymean , 'b--' , linewidth=2)
    
        max_record = (y == ymax)
        min_record = (y == ymin)
        plt.plot(x[max_record], y[max_record] - ymean[max_record], 'kx', markersize=10)
        plt.plot(x[min_record], y[min_record] - ymean[min_record], 'kx', markersize=10)
    except:
        pass
        plt.text(3,0.5, 'Invalid data')
        
def repnone(x):
    for i, val in enumerate(x):
        if  val == None:
            x[i] = np.nan
    return x        

def plotMonth(request, station_id, variable_id, selected_year, selected_month):
    """ Plot the monthly results"""
    s = get_object_or_404(Station, pk=station_id) # Get the station object from django
    v = get_object_or_404(Variable, pk=variable_id) # Get the variable object from django
    
    # Get the monthly mean data from the database, make into dataframe
    qsm = MonthlyMean.objects.filter(variable=v, station=s)
    tmt = repnone(np.array([ ob.value for ob in qsm ]))
    tmtime = repnone(np.array([ ob.time for ob in qsm ]))
    dfmon = pd.DataFrame( tmt, columns=['tmt'], index = tmtime )
    current_year = int( selected_year )
    mean_cy = dfmon[dfmon.index.year==current_year]
    mean_cy_data = mean_cy['tmt'].values
  
    if len( mean_cy_data) == 0:
        mean_cy_data = np.arange(1,13)#*np.nan
    
    # Get the monthly climatology
    qsmclim = MonthlyClim.objects.filter(variable=v, station=s)
    mclim_mean = repnone( np.array([ ob.mean for ob in qsmclim ]) )
    mclim_std = repnone( np.array([ ob.std for ob in qsmclim ]) )
    mclim_max = repnone(np.array([ ob.max for ob in qsmclim ]) )
    mclim_min = repnone(np.array([ ob.min for ob in qsmclim ]) )
    mclim_month = repnone( np.array([ ob.month for ob in qsmclim ]) )

    # make the plots
    fig, axa = plt.subplots(nrows=1,ncols=2, subplot_kw=dict(axisbg=[0.9, 0.9, 0.9]))
    fig.set_size_inches(12,4)
    axa=axa.flatten()
    # Temperature
    # Tmean
    plt.sca( axa[0] )
    
    pltmean( mclim_month, mclim_mean )
    plt.title('Monthly averages', fontsize = 16 )
    plt.text(2, mclim_mean.max()*1.2,\
    v.name, fontsize=14, fontweight='bold')
    
    # Tmean anomalies for current_year
    plt.sca( axa[1] )
    pltanom(mclim_month, repnone(mean_cy_data), mclim_mean, mclim_max, mclim_min, mclim_std)
    print type(mclim_month)
    plt.title( str( current_year )+' anomalies', fontsize = 16 )
    
    for ax in axa:
        ax.grid(color='white', linestyle='solid')
        fig.set_facecolor('w')
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.get_xaxis().tick_bottom()
        ax.get_yaxis().tick_left()
        ax.set_xticks( np.arange(1,13) )
        ax.set_xlim([ 0.5 , 12.5 ] )
        ax.set_axisbelow(True)

        #ax.set_xticklabels([ 'Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec' ] , rotation=45)
        ax.set_xticklabels([ 'Jan','','Mar','','May','','Jul','','Sep','','Nov',''] , rotation=45)
    
    # return the appropriate image response
    canvas = FigureCanvas(fig)
    response = HttpResponse(content_type='image/png')
    canvas.print_png(response)
    return response

def plotYear(request, station_id, variable_id, selected_year, selected_month):
    """ Plot the Annual results"""
    # Get the station and variable objects
    s = get_object_or_404(Station, pk=station_id) 
    v = get_object_or_404(Variable, pk=variable_id) 

    # Get the annual mean data from the database and plot
    qsa = AnnualMean.objects.filter(variable=v, station=s)
    tmtann = [ ob.value for ob in qsa ]
    tmtimeann = [ ob.time for ob in qsa ]
    dfann = pd.DataFrame( tmtann, columns=['tmt'], index = tmtimeann )
    
        # make the plots
    fig, axa = plt.subplots(nrows=1,ncols=1, subplot_kw=dict(axisbg=[0.9, 0.9, 0.9]))    
    fig.set_size_inches(12,6)
    
    # Tmean anomalies for current_year
    pltanom(dfann.index.year, dfann['tmt'].values, dfann['tmt'].mean(),dfann['tmt'].max(), dfann['tmt'].min(), dfann['tmt'].std() )
    plt.title( 'Anomaly from mean (' + str( np.round(dfann['tmt'].mean(), 2) ) + v.unit + ')', fontsize = 16 )
    
    for ax in [axa]:
        ax.grid(color='white', linestyle='solid')
        fig.set_facecolor('w')
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.get_xaxis().tick_bottom()
        ax.get_yaxis().tick_left()
        ax.autoscale(enable=True, axis='x', tight=True)
        ax.set_axisbelow(True)
        ax.set_xlabel('Year')
    
    # return the appropriate image response
    canvas = FigureCanvas(fig)
    response = HttpResponse(content_type='image/png')
    canvas.print_png(response)
    return response

def plotDay(request, station_id, variable_id, selected_year, selected_month):
    """ Plot the Annual results"""
    # Get the station and variable objects
    s = get_object_or_404(Station, pk=station_id) 
    v = get_object_or_404(Variable, pk=variable_id) 

    # Get the Daily mean data from the database, make into dataframe
    qsm = DailyMean.objects.filter(variable=v, station=s)
    tmt = repnone(np.array([ ob.value for ob in qsm ]))
    tmtime = repnone(np.array([ ob.time for ob in qsm ]))
    df = pd.DataFrame( tmt, columns=['tmt'], index = tmtime )
    current_year = int( selected_year )
    current_month = int( selected_month )
    dfcm = df[(df.index.year==current_year) & (df.index.month==current_month )]
    dfcm_data = dfcm['tmt'].values.squeeze()

    # Deal with non leap-years for Feb
    if current_month == 2:
        if len(dfcm_data) == 28:
            dfcm_data = np.append( dfcm_data, np.nan )
    
    # Get the Daily climatology
    qsmclim = DailyClim.objects.filter(variable=v, station=s, month=current_month)
    mclim_mean = repnone( np.array([ ob.mean for ob in qsmclim ]) )
    mclim_std = repnone( np.array([ ob.std for ob in qsmclim ]) )
    mclim_max = repnone(np.array([ ob.max for ob in qsmclim ]) )
    mclim_min = repnone(np.array([ ob.min for ob in qsmclim ]) )
    mclim_day = repnone( np.array([ ob.day for ob in qsmclim ]) )
            
        # make the plots
    fig, axa = plt.subplots(nrows=1,ncols=1, subplot_kw=dict(axisbg=[0.9, 0.9, 0.9]))    
    fig.set_size_inches(12,6)    

    #pltmean( dfcy.index.dayofyear, dfcy['tmt'] ,line=True)
    pltanom( mclim_day, dfcm_data, mclim_mean, mclim_max, mclim_min, mclim_std)
    plt.title('Daily averages for ' + selected_year, fontsize = 16 )
    plt.text(2, dfcm['tmt'].max()*1.2,\
    v.name, fontsize=14, fontweight='bold')
    
    month =['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August'\
            , 'September', 'October', 'November', 'December']
    for ax in [axa]:
        ax.grid(color='white', linestyle='solid')
        fig.set_facecolor('w')
        ax.spines['top'].set_visible(False)
        ax.spines['right'].set_visible(False)
        ax.get_xaxis().tick_bottom()
        ax.get_yaxis().tick_left()
        ax.autoscale(enable=True, axis='x', tight=True)
        ax.set_axisbelow(True)
        ax.set_xlabel('Day of ' + month[current_month -1] )
    
    # return the appropriate image response
    canvas = FigureCanvas(fig)
    response = HttpResponse(content_type='image/png')
    canvas.print_png(response)
    return response

def showStaticImage(request):
    """ Simply return a static image as a png """

    imagePath = "/Users/neil/websites/vicweather/django/visualweather/stations/static/stations/images/tux.png"
    from PIL import Image
    Image.init()
    i = Image.open(imagePath)

    response = HttpResponse(mimetype='image/png')
    i.save(response,'PNG')
    return response
