from django.conf.urls import patterns, url
from stations import views

urlpatterns = patterns('',
    url(r'^$', views.IndexView.as_view(), name='index'),
    #url(r'^(?P<pk>\d+)/$', views.DetailView.as_view(), name='detail'),
    url(r'^(?P<pk>\d+)/$', views.stationRedirect, name='detail'),
    url(r'^(?P<station_id>\d+)$', views.vote, name='vote'),
    #url(r'^(?P<pk>\d+)/(?P<variable_id>\d+)/results/$', views.ResultsView.as_view(), name='results'),
    url(r'^(?P<pk>\d+)/(?P<variable_id>\d+)/(?P<selected_year>\d+)/(?P<selected_month>\d+)/(?P<av_per>\w+)/$'\
         , views.resultsView2, name='results'),
    url(r'^(?P<station_id>\d+)/(?P<variable_id>\d+)/(?P<selected_year>\d+)/(?P<selected_month>\d+)/day/result.png$', views.plotDay),
    url(r'^(?P<station_id>\d+)/(?P<variable_id>\d+)/(?P<selected_year>\d+)/(?P<selected_month>\d+)/month/result.png$'
         , views.plotMonth),
    url(r'^(?P<station_id>\d+)/(?P<variable_id>\d+)/(?P<selected_year>\d+)/(?P<selected_month>\d+)/year/result.png$'
         , views.plotYear),
    url(r'^background.jpg$', views.showStaticImage )
)
