from django.db import models

# Create your models here.
class Station(models.Model):
    name = models.CharField(max_length=200, unique=True, null=True)
    tcid = models.CharField(max_length=200,unique=True, null=True)
    wmoid = models.IntegerField(unique=True, null=True)
    latitude = models.FloatField(default=-999)
    longitude = models.FloatField(default=-999)
    elevation = models.FloatField(default=-999)
    province = models.CharField(max_length=200)
    def __unicode__(self):  
        return self.name
    
class Variable(models.Model):
    station = models.ManyToManyField(Station)
    name = models.CharField(max_length=200,unique=True)
    unit = models.CharField(max_length=200)

    def __unicode__(self):  
        return self.name

class DailyMean(models.Model):
    station = models.ForeignKey(Station)
    variable = models.ForeignKey(Variable)
    time = models.DateTimeField(default=-999)
    value = models.FloatField(default=-999, null=True)
    def __unicode__(self):  
        return 'Time: ' + self.time.strftime('%Y/%m/%d') + ' ' + self.variable.name + ' ' + str(self.value)
    
class MonthlyMean(models.Model):
    station = models.ForeignKey(Station)
    variable = models.ForeignKey(Variable)
    time = models.DateTimeField(default=-999)
    value = models.FloatField(default=-999, null=True)
    def __unicode__(self):  
        return 'Time: ' + self.time.strftime('%Y/%m/%d') + ' ' + self.variable.name + ' ' + str(self.value)
    
class AnnualMean(models.Model):
    station = models.ForeignKey(Station)
    variable = models.ForeignKey(Variable)
    time = models.DateTimeField()
    value = models.FloatField(default=-999, null=True)

class DailyClim(models.Model):
    station = models.ForeignKey(Station)
    variable = models.ForeignKey(Variable)
    month = models.IntegerField()
    day = models.IntegerField()
    mean = models.FloatField(default=-999, null=True)
    std = models.FloatField(default=-999, null=True)
    min = models.FloatField(default=-999, null=True)
    max = models.FloatField(default=-999, null=True)

class MonthlyClim(models.Model):
    station = models.ForeignKey(Station)
    variable = models.ForeignKey(Variable)
    month = models.IntegerField()
    mean = models.FloatField(default=-999, null=True)
    std = models.FloatField(default=-999, null=True)
    min = models.FloatField(default=-999, null=True)
    max = models.FloatField(default=-999, null=True)
