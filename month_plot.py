# month_plot.py
""" 
Loads EC weather station data and uses matplotlib to produce some 
figures. All of the number crunching is done though functions saved
in the 'procweatherdata' model. 

The name is misleading. Monthly, daily and annual mean data are plotted.

This is the main controller for now.
--------------------------------------------------------------------------------
"""

import numpy as np
import procweatherdata as prwd
import matplotlib.pyplot as plt
import datetime as datetime
import subpltset as sps

# Get rid of previous plots, and make plotting interactive (ipython)
plt.close('all')
plt.ion()
plt.hold('True')

# Define some basics about the station data
station_prefix = 'yyj'
startyear = 1940
endyear = 2012
# Define a current_year for the anomaly calc. Defaults to most recent year
# but ultimately it would be nice for this to be user controlled.
current_year = endyear

# Load the data and print out the headers
headers , data = prwd.loadstn( station_prefix + '_daily_' , startyear , endyear )
for i in range( len(headers) ):
    print i ,  headers[i]

#--------------------------------------------------------------------------------------
# Define some plotting functions that we will use repeatedly. Easier to define them
# once, since changes only have to be made here and will be applied to all plots.
#--------------------------------------------------------------------------------------

def pltmean( x , ymean ):
     """ Black bar plots for the mean """
     plt.bar( x , ymean , facecolor='black',\
     align = 'center',edgecolor='white')
     plt.ylim([ 0 , np.max(ymean) * 1.50 ] )

def pltanom( x , y , ymean , ymax , ymin , ystd):
    """ Anomaly plots, includingn a grey envelope for the std-dev (ystd), colored bars
    for the anomalies (y - ymean) and dashed colored lines for the extremes (ymin/max). """
    xe = np.linspace( min(x) -1 , max(x) +1 , len(x) )
    plt.fill_between( xe , -1.0 * ystd ,\
    ystd , facecolor = 'black', alpha = 0.15)
    plt.plot( xe , np.zeros(len(xe)) , 'k-')

    yanom = y - ymean
    neg = yanom < 0.
    pos = yanom >= 0.
    
    plt.bar( x[ neg ], yanom[ neg ], facecolor='#9999ff', edgecolor='white',\
     align = 'center')
    plt.bar( x[ pos ] , yanom[ pos ], facecolor='#ff9999', edgecolor='white'\
    , align = 'center')

    plt.plot( x , ymax - ymean , 'r--' , linewidth=2)
    plt.plot( x , ymin - ymean , 'b--' , linewidth=2)


#--------------------------------------------------------------------------------------
# Plot the monthly data
#--------------------------------------------------------------------------------------

# Calculate the monthly means, std's and min / max (over all months).
monthly_means , monthly_stds = prwd.climatology( prwd.monthly_means( data ) )
monthly_mins , monthly_max = prwd.minmax( prwd.monthly_means( data ) )

# Get the monthly mean data for the current_year
mean_cy  = prwd.monthly_means( prwd.getyear( data , current_year) )

# Define x-axis ( months )
xd = np.arange( 1 , 13 , 1 )
xde = np.linspace( 0 , 13 , 12 )

# Call sps to setup subplots with handle ax
ax = sps.subpltset( 3 , 2 )

# Temperature
# Tmean
plt.sca( ax[0] )
pltmean( xd , monthly_means[ : , 9 ] )
plt.title('Monthly averages', fontsize = 16 )
plt.text( 2 , max(monthly_means[ : , 9 ])*1.2 ,\
 'Tmean' , fontsize=14 , fontweight='bold')

# Tmean anomalies for current_year
plt.sca( ax[1] )
pltanom( xd , mean_cy[ : , 9 ], monthly_means[ : , 9 ], monthly_max[:,9],\
 monthly_mins[:,9] , monthly_stds[:,9] )
plt.title( str( current_year )+' anomalies', fontsize = 16 )

# Tmin
plt.sca( ax[2] )
pltmean( xd , monthly_means[ : , 7 ] )
plt.ylabel(r'Temp. ($^{\circ}$C)')
plt.text( 2 , max(monthly_means[ : , 7 ])*1.2 ,\
 'Tmin' , fontsize=14 , fontweight='bold')

# Tmin anomalies for current year
plt.sca( ax[3] )
pltanom( xd , mean_cy[ : , 9 ], monthly_means[ : , 9 ], monthly_max[:,9],\
 monthly_mins[:,9] , monthly_stds[:,9] )

# Tmax
plt.sca( ax[4] )
pltmean( xd , monthly_means[ : , 5 ]  )
plt.ylabel(r'Temp. ($^{\circ}$C)')
plt.text( 2 , max(monthly_means[ : , 5 ])*1.2\
 , 'Tmax' , fontsize=14 , fontweight='bold')

# Tmax anomalies for current year
plt.sca( ax[5] )
pltanom( xd , mean_cy[ : , 9 ], monthly_means[ : , 9 ], monthly_max[:,9],\
 monthly_mins[:,9] , monthly_stds[:,9] )

# Do some axis limit control and labeling.  
for i in range( len( ax ) ) :
   ax[i].set_xticks( np.arange(1,13) )
   ax[i].set_xlim([ 0.5 , 12.5 ] )
   if (i+1) < 5:
       ax[i].set_xticklabels([])
   else:
       ax[i].set_xticklabels([ 'Jan','Feb','Mar',\
       'Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec' ] , rotation=90)

# Rainfall
plt.figure()

# Calculate the monthly sums for rainfall, and the mean of the monthly
# summed rainfall over all years, as well as the std and min/max.
monthly_sums = prwd.monthly_sums( data )
monthly_sum_mean, monthly_sum_std = prwd.climatology( monthly_sums )
monthly_sum_min , monthly_sum_max = prwd.minmax( monthly_sums )

# Get monthly sums for the current_year
msum_cy = prwd.monthly_sums(prwd.getyear( data , current_year ) )

plt.subplot(321)

# Monthly mean total precip.
pltmean( xd , monthly_sum_mean[ : , 19 ] )
plt.title('Monthly averages', fontsize = 18 )
plt.ylabel(r'Precip. (mm)')
plt.xlabel('Month')
plt.gca().set_xticklabels([ 'Jan','Feb','Mar',\
'Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec' ] , rotation=90)
plt.text( 2 , max(monthly_sum_mean[ : , 19 ])*1.2 ,\
 'Total precip.' , fontsize=14 , fontweight='bold')
plt.xticks( np.arange(1,13) )
plt.xlim([ 0.5 , 12.5 ] )


# Total precip. anomalies for the current_year
plt.subplot(322)
pltanom( xd , msum_cy[:,19], monthly_sum_mean[:,19], monthly_sum_max[:,19],\
 monthly_sum_min[:,19], monthly_sum_std[:,19])
plt.plot( xde , np.zeros(len(xd)) , 'k-')
plt.xlabel('Month')
plt.gca().set_xticklabels([ 'Jan','Feb','Mar',\
'Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec' ] , rotation=90)
plt.title( str( current_year )+' anomalies', fontsize = 16 )
plt.xticks( np.arange(1,13) )
plt.xlim([ 0.5 , 12.5 ] )

#--------------------------------------------------------------------------------------
# Do daily
#--------------------------------------------------------------------------------------

# Select a monthly to look at (ultimately should be all / variable / user selected)
month = 12

# Compute daily means over all 366 possible year days
daily_mean_all , daily_std_all = prwd.daily_climatology( data )
daily_means = daily_mean_all[ daily_mean_all[ : , 2] ==month , :  ]
daily_stds = daily_std_all[ daily_mean_all[ : , 2] == month , :  ]

# Compute daily extreme values for month month 
daily_mins , daily_max = prwd.daily_minmax( data , month)

cm_mask = ( data[ : , 1 ] == endyear ) & ( data[ : , 2 ] == month )
mean_cm = data[ cm_mask , : ]

# Define the x-axis in days
xd = np.arange( 1 , len(daily_means) + 1 , 1 )
# this second axis is a bit wider so that the envelopes reach the edges.
xde = np.linspace( 0 , len(daily_means) + 2 , len(daily_means)  )

# Temperature
# Tmean daily climatological mean
plt.figure()
ax3 = sps.subpltset( 3 , 2 )
plt.sca( ax3[0] )
pltmean( xd , daily_means[ : , 9 ] )
plt.title('Daily averages', fontsize = 16 )
plt.text( 2 , max(daily_means[ : , 9 ])*1.2 ,\
 'Tmean' , fontsize=14 , fontweight='bold')
plt.xlim([ 0.5 , 31.5 ] )

# Tmean daily anomaly for current_year
plt.sca( ax3[1] )
pltanom( xd , mean_cm[ : , 9 ], daily_means[ : , 9 ], daily_max[ : ,9],\
 daily_mins[ : , 9 ] , daily_stds[:,9] )
plt.title( str( current_year ) + ' anomalies', fontsize = 16 )
plt.xticks( np.arange(1,31,4) )
plt.xlim([ 0.5 , 31.5 ] )
plt.plot( xde , np.zeros(len(xd)) , 'k-')

# Tmin daily climatological mean
plt.sca( ax3[2] )
pltmean( xd , daily_means[ : , 7 ] )
plt.text( 2 , max(daily_means[ : , 7 ])*1.2 ,\
 'Tmin' , fontsize=14 , fontweight='bold')
plt.xticks( np.arange(1,31,4) )
plt.xlim([ 0.5 , 31.5 ] )

# Tmin daily anomaly for current_year
plt.sca( ax3[3] )
pltanom( xd , mean_cm[ : , 7 ], daily_means[ : , 7 ], daily_max[ : , 7 ],\
 daily_mins[  :  , 7 ] , daily_stds[ : , 7 ] )
plt.xticks( np.arange(1,31,4) )
plt.xlim([ 0.5 , 31.5 ] )
plt.plot( xde , np.zeros(len(xd)) , 'k-')


# Tmax daily climatological mean
plt.sca( ax3[4] )
pltmean( xd , daily_means[ : , 5 ] )
plt.text( 2 , max(daily_means[ : , 5 ])*1.2 ,\
 'Tmax' , fontsize=14 , fontweight='bold')
plt.xticks( np.arange(1,31,4) )
plt.xlim([ 0.5 , 31.5 ] )
plt.xlabel('Day (Dec)')

# Tmax daily anomaly for current_year
plt.sca( ax3[5] )
pltanom( xd , mean_cm[ : , 5 ], daily_means[ : , 5 ], daily_max[ : , 5 ],\
 daily_mins[  :  , 5 ] , daily_stds[ : , 5 ] )
plt.xticks( np.arange(1,31,4) )
plt.xlim([ 0.5 , 31.5 ] )
plt.xlabel('Day (Dec)')
plt.plot( xde , np.zeros(len(xd)) , 'k-')

# Do some axis re-jigs.
for i in range( len( ax3 ) ) :
   ax3[i].set_xticks( np.arange(1,31,4) )
   ax3[i].set_xlim([ 0.5 , 31.5 ] )
   if (i+1) < 5:
       ax3[i].set_xticklabels([])

#--------------------------------------------------------------------------------------
# Do some simple annual mean time-series. Definitely room for improvement. Could
# plot time-series for the current_month. Could also add in a little trend line
# and maybe some other stuff. Maybe even a histogram could be thrown in.
#--------------------------------------------------------------------------------------
# make the time arrays
mt = prwd.gettime( data )
ann_mean = prwd.annual_mean( data )
amt = prwd.gettime( ann_mean )

plt.figure()
plt.subplot(221)
dt,=plt.plot( mt , data[ :, 7]  )
at,=plt.plot( amt , ann_mean[ :, 7] ,'r-' , linewidth = 3  )
plt.title('Daily temperature', fontsize =16)
plt.ylabel(r'Temp. ($^{\circ}$C)')
plt.legend([ dt , at ] , ['Daily mean' , 'Annual mean'], loc=3)
plt.xlabel('Date')
plt.xticks( rotation = 45 )

plt.subplot(222)
mtm = prwd.gettime( monthly_sums )
dtr,=plt.plot( mtm , monthly_sums[:, 19]  )
ann_mean_mon_sum = prwd.annual_mean( monthly_sums )
am_sumt = prwd.gettime( ann_mean_mon_sum )
atr,=plt.plot( am_sumt , ann_mean_mon_sum[ :, 19] ,'r-' , linewidth = 3 )
plt.title('Monthly rainfall', fontsize =16)
plt.ylabel('Precip. (mm)')
plt.xlabel('Date')
plt.legend([ dtr , atr ] , ['Monthly' , 'Annual mean'], loc=2)
plt.xticks( rotation = 45 )
