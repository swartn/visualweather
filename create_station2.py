from stations.models import Station, Variable, DailyClim, DailyMean, MonthlyMean, AnnualMean, MonthlyClim
import datetime as datetime
import pandas as pd
import re
import glob

def getheader(infile):
    """Load the first 8 lines of file header from an EC weather .csv data file into a dict and return it
    """
    pdict={}
    with open(infile,'r') as f:
        for l in range(8):
            line = f.next()
            pdict[ line.split(',')[0] ] =line.split(',')[1].strip()
            
    return pdict
        
def loadyr(infile):
    """ Load the data for a single year given in infile into a pandas dataframe. Do some processing
    of the columns to give reasonable labels, and also compute a datetime index from the dates and
    set as the index. Finally drop some unwanted columns and the return the dataframe.
    """
    df = pd.read_csv(infile, skiprows=24, parse_dates={'Date':[0]})
    df.index = df.Date
    dcol = [ re.sub(r'\(.*?\)', '', col).strip(' ').replace(' ', '_') for col in df.columns ]
    df.columns = dcol
    del_list = [ col for col in dcol if col.split('_')[-1] == 'Flag' ]
    [ del_list.append(v) for v in ['Year', 'Month', 'Day', 'Date'] ]
    df = df.drop(del_list,axis=1)
    return df

def loadall(path, startyear='', endyear=''):
    """Load the data contained in path/*.csv into a single pandas dataframe, and return it.
    """
    if startyear == '':
        yrs = [yr.split('_')[-1].split('.')[0] for yr in glob.glob(path + '/*.csv') ]
        startyear = int(yrs[0])
        endyear = int(yrs[-1])
        
    for k, infile in enumerate( glob.glob(path + '/*.csv') ):
        if k == 0:
            print infile
            df = loadyr(infile)
            header = getheader(infile)
        else:
            dft = loadyr(infile)
            df = df.append(dft) 
    return df, header
    
def initdb(variables, units):
    """ Initialize the variables for a new visualweather database.
    """
    for i, var in enumerate( variables ):
        v = Variable( name=var, unit=units[i] )
        v.save()
    
def newstntodb(path, prefix, variables):
    """ Load data for a new station, compute it's statistics, and save all the
    data to the database for use in django
    """
    # load the data
    data, pdict = loadall( path )

    # create the station 
    s = Station(name=pdict['Station Name'], tcid=pdict['TC Identifier'], wmoid=pdict['WMO Identifier'] \
                , latitude=pdict['Latitude'], longitude=pdict['Longitude'], elevation=pdict['Elevation'] \
                , province=pdict['Province'])
    s.save()

    # Add the variables to this station
    for i, var in enumerate( variables ):
        v = Variable.objects.get(name=var)
        v.station.add(s)
        
    # Do bulk creation of daily data
    for i, var in enumerate( variables ):
        ds = []
        v = Variable.objects.get(name=var)
        times = data[var].index
        for k, dp in enumerate( data[var] ):
            ds.append( DailyMean(station=s, variable=v, time=times[k], value=dp) )
            
        DailyMean.objects.bulk_create( ds )
      
     # Do bulk creation of Monthly data
    for i, var in enumerate( variables ):
        ds = []
        v = Variable.objects.get(name=var)
        times = data[var].resample('M').dropna().index
        for k, dp in enumerate( data[var].resample('M').dropna() ):
            ds.append( DailyMean(station=s, variable=Variable.objects.get(name=var) \
                                  , time=times[k], value=dp) )
            
        MonthlyMean.objects.bulk_create( ds )
        
     # Do bulk creation of Annual data
    for i, var in enumerate( variables ):
        ds = []
        v = Variable.objects.get(name=var)
        times = data[var].resample('A').dropna().index
        for k, dp in enumerate( data[var].resample('A').dropna() ):
            ds.append( DailyMean(station=s, variable=Variable.objects.get(name=var) \
                                  , time=times[k], value=dp) )
            
        AnnualMean.objects.bulk_create( ds )       

    # Do bulk creation of Monthly Climatology
    monthly_means = data.resample('M')
    monthly_clim = data.groupby( data.index.month ).mean()
    monthly_std = monthly_means.groupby( monthly_means.index.month ).std()
    monthly_min = monthly_means.groupby( monthly_means.index.month ).min()
    monthly_max = monthly_means.groupby( monthly_means.index.month ).max()
    
    for i, var in enumerate( variables ):
        ds = []
        for k in range(1,13):
            ds.append( MonthlyClim(station=s, variable=Variable.objects.get(name=var), month=k \
                                    , mean=monthly_clim[var][k], std=monthly_std[var][k] \
                                    , max=monthly_max[var][k],  min=monthly_min[var][k]) )
            
        MonthlyClim.objects.bulk_create( ds )
        
    # Do bulk creation of Daily Climatology
    grouped    = data.groupby( [data.index.month, data.index.day ])
    daily_clim = grouped.mean()
    daily_std  = grouped.std()
    daily_min  = grouped.min()
    daily_max  = grouped.max()
    
    for i, var in enumerate( variables ):
        ds = []
        for month, day in daily_clim.index:
            ds.append( DailyClim(station=s, variable=Variable.objects.get(name=var) \
                                    , month=month, day=day \
                                    , mean=daily_clim[var][month, day], std=daily_std[var][month, day] \
                                    , max=daily_max[var][month, day],  min=daily_min[var][month, day]) )
            
        DailyClim.objects.bulk_create( ds ) 

if __name__ == "__main__" :
     #  Decide on the projects main variables
     variables = ['Mean_Temp', 'Max_Temp', 'Min_Temp', 'Total_Precip', 'Total_Snow']
     units = ['^oc', '^oc', '^oc', 'mm', 'mm']
     
     # initialize the database
     if True:
        initdb(variables, units)

     # create station wyj and load data
     pref = 'wyj'
     path = '/Users/neil/vicweather/wyj/'   
     newstntodb( path, pref,variables)

     # create station yyj and load data
     pref = 'yyj'
     path = '/Users/neil/vicweather/yyj/'   
     newstntodb( path, pref, variables)



