import numpy as np
import datetime as datetime
import glob
import re
import pandas as pd


def loadstn(path, name_prefix , startyear='', endyear=''):
    """
    Loads in the daily-mean EC weather data from csv files. Each csv file contains
    one year of data, and obeys the name-convention 'name_prefix_year.csv'. The script 
    loops over all years from startyear to endyear and combines them into a single numpy 
    array called data. It also returns the headers: headers , data = loadstn(....
    """
    if startyear == '':
       yrs = [yr.split('_')[-1].split('.')[0] for yr in glob.glob(path + '/*.csv') ]
       startyear = int(yrs[0])
       endyear = int(yrs[-1])

    name = path + name_prefix + str( startyear ) + '.csv'
    A = np.loadtxt( name , skiprows = 23 , delimiter = ',' , dtype = str )
    headers = A[ 0 , : ]

    pdict={}
    with open(name,'r') as f:
        for l in range(8):
            line = f.next()
            pdict[ line.split(',')[0] ] =line.split(',')[1].strip()

    data = np.genfromtxt( name , delimiter = ',' , dtype= 'float',\
    skiprows = 25 )

    for y in range( startyear + 1 , endyear + 1 ):  
       name = path + name_prefix + str(y) + '.csv'
       data = np.append( data ,\
       np.genfromtxt( name , delimiter = ',' , dtype = 'float' , \
       skiprows = 25 ) , axis = 0  )

    dtime = gettime(data)
    dcol = [ re.sub(r'\(.*?\)', '', col).strip(' ').replace(' ', '_') for col in headers ]

    dataf = pd.DataFrame(data, columns=dcol, index=dtime )
    del_list = [ col for col in dcol if col.split('_')[-1] == 'Flag' ]
    dataf = dataf.drop(del_list,axis=1)

    return headers, dataf, pdict

def getmonth( data , month ):
    """ From data, find and return all rows which correspond to a given month. """
    monthdata = data[ data[ : , 2 ] == month , : ] ;
    return monthdata

def getyear( data , year ):
    """ From data, find and return all rows which correspond to a given year. """
    yeardata = data[ data[ : , 1 ] == year , : ] ;
    return yeardata

def getmonthofyear( data , year , month ):
    """ From data, find and return all rows which correspond to a given month in a 
    given year. """
    yeardata = getyear( data , year ) ;
    moydata = getmonth ( yeardata , month ) ;
    return moydata


def climatology( data ):
    """ From data, compute and return the monthly climatology (mean for each of the 12 months over
    all years) and monthly standard deviation computed over all rows """
    nt , ne = data.shape
    monthly_mean = np.zeros([ 12 , ne]) ; 
    monthly_std = np.zeros([ 12 , ne]) ; 
    for m in range( 1 , 13):
        month_data = data[ data[ : , 2 ] == m , : ] 
        """ Do a check for too many missing values """
	if ( sum( np.isnan(month_data[:,9]) )  > 20 ): 
            month_data[:,5:] = np.nan	
        masked_data = np.ma.masked_array( month_data , np.isnan( month_data ) )
        mm = np.mean( masked_data , axis = 0 )
        mstd = np.std( masked_data , axis = 0 )
        monthly_mean[ m - 1 , : ] = mm.filled( np.nan ) 
        monthly_std[ m - 1 , : ] = mstd.filled( np.nan ) 

    return monthly_mean, monthly_std 


def daily_climatology( data ):
    """ From data, compute and return the daily climatology (mean for each of the
    366 days over all years) and daily standard deviation over all rows """
    nt , ne = data.shape
    daily_mean = np.zeros([ 366 , ne]) ; 
    daily_std = np.zeros([ 366 , ne]) ;
    count = 0
    for m in range( 1 , 13):
        month_data = getmonth( data , m)
        for d in range( int( max( month_data[ : , 3 ] )  ) ):
            day_data = month_data[ month_data[ : , 3 ] == d + 1 , : ] ;
            masked_data = np.ma.masked_array( day_data ,\
             np.isnan( day_data ) )
            dm = np.mean( masked_data , axis = 0 )
            dstd = np.std( masked_data , axis = 0 )
            daily_mean[ count  , : ] = dm.filled( np.nan ) 
            daily_std[ count  , : ] = dstd.filled( np.nan ) 
            count = count + 1

    return daily_mean, daily_std 

def monthly_means( data ):
    """ Computes and return the monthly mean, for each month in data"""
    nt , ne = data.shape
    startyear = np.min( data[ : , 1 ] )
    endyear = np.max( data[ : , 1 ] )
    nyrs = int( endyear - startyear + 1 )
    all_months_mean = np.zeros( [ nyrs * 12  , ne ] )

    for y in range( nyrs ):
        for m in range( 1 , 13):
            moy_data = getmonthofyear( data , startyear + y , m )
	    if ( sum( np.isnan(moy_data[:,9]) )  > 20 ):
                moy_data[:,5:] = np.nan	
 	
            masked_data = np.ma.masked_array( moy_data ,\
            np.isnan( moy_data ) )
            masked_data = np.ma.masked_array( moy_data ,\
            np.isnan( moy_data ) )
            mmean = np.mean( masked_data , axis = 0 )
            all_months_mean[ y*12 + m - 1 , : ]  = mmean.filled( np.nan )  
            all_months_mean[ y*12 + m - 1 , 0 : 4 ]  = moy_data[ 0 , 0 : 4 ]  
    return all_months_mean

    
def minmax( data ):
    """ Computes and return the min and max, for each month in data"""
    nt , ne = data.shape
    monthly_min = np.zeros([ 12 , ne]) ; 
    monthly_max = np.zeros([ 12 , ne]) ; 
    for m in range( 1 , 13):
        month_data = data[ data[ : , 2 ] == m , : ] 
        masked_data = np.ma.masked_array( month_data , np.isnan( month_data ) )
        mmin = np.min( masked_data , axis = 0 )
        mmax = np.max( masked_data , axis = 0 )
        monthly_min[ m - 1 , : ] = mmin.filled( np.nan ) 
        monthly_max[ m - 1 , : ] = mmax.filled( np.nan ) 

    return monthly_min , monthly_max

def daily_minmax( data , month ):
    """ Computes and return the min and max, for each day of the month in data.
    This function is expecting data to correspond to one month."""
    nt , ne = data.shape
    daily_min = np.zeros([ 31 , ne]) ; 
    daily_max = np.zeros([ 31 , ne]) ;
    count = 0

    month_data = data[ data[ : , 2 ] == month , : ] 

    for d in range( int( max( month_data[ : , 3 ] )  ) ) :
            day_data = month_data[ month_data[ : , 3 ] == d + 1 , : ] ;
            if np.shape( day_data[:,1]) > 0:
                 masked_data = np.ma.masked_array( day_data ,\
                  np.isnan( day_data ) )
                 dmin = np.min( masked_data , axis = 0 )
                 dmax = np.max( masked_data , axis = 0 )
                 daily_min[ count  , : ] = dmin.filled( np.nan ) 
                 daily_max[ count  , : ] = dmax.filled( np.nan ) 
                 count = count + 1

    return daily_min, daily_max 

def monthly_sums( data ):
    """ The sum over all days for each month for all months in data """
    nt , ne = data.shape
    startyear = np.min( data[ : , 1 ] )
    endyear = np.max( data[ : , 1 ] )
    nyrs = int( endyear - startyear + 1 )
    monthly_sums = np.zeros( [ nyrs * 12  , ne ] )


    for y in range( nyrs ):
        for m in range( 1 , 13):
            moy_data = getmonthofyear( data , startyear + y , m )
	    if ( sum( np.isnan(moy_data[:,19]) )   > 20 ):
                moy_data[:,5:] = np.nan	
 	
            masked_data = np.ma.masked_array( moy_data ,\
            np.isnan( moy_data ) )
            msum = np.sum( masked_data , axis = 0 )
            monthly_sums[ y*12 + m - 1 , : ]  = msum.filled( np.nan )  
            monthly_sums[ y*12 + m - 1 , 0 : 4 ]  = moy_data[ 0 , 0 : 4 ]  
    return monthly_sums


def gettime( data ):
    """ Turn the year, month, day fields in data into a list of python datetime elements """
    stn_time = []   
    [ stn_time.append( datetime.datetime( int( data[ i ,1 ] ) ,\
    int(data[ i , 2]) , int( data[ i , 3 ] ) ) )   \
    for i in range( len( data ) ) ] 
    
    return stn_time


def annual_mean( data ):
    """ Compute the annual mean for each year in data """
    nt , ne = data.shape
    startyear = np.min( data[ : , 1 ] )
    endyear = np.max( data[ : , 1 ] )
    nyrs = int( endyear - startyear + 1 )
    annual_mean = np.zeros( [ nyrs  , ne ] )

    for y in range( nyrs ):
            yeardata = getyear( data , startyear + y  )
            masked_data = np.ma.masked_array( yeardata ,\
            np.isnan( yeardata ) )
            amean = np.mean( masked_data , axis = 0 )
            annual_mean[ y , : ]  = amean.filled( np.nan )  
    return annual_mean


