from stations.models import Station, Variable, Data
import datetime
s = Station(name="VICTORIA INT'L A", tcid="YYJ", wmoid=71799)
s.save()

s.variable_set.create(name='tmean', unit='^oC')

s = Station.objects.get(id=1)
v = Variable.objects.get(name="tmean")
#e = Data(station=s,variable=v,time=datetime.datetime(2001,1,1),value=2)
#e.save(force_insert=True)

nd = []
for i in range(100):
  nd.append( Data(station=s, variable=v, time=datetime.datetime(1900+i,1,1), value=20 + i*0.01) ) 
Data.objects.bulk_create( nd )

  #e2 = s.data_set.create(variable=v,time=datetime.datetime(1900+i,1,1),value=20 + i*0.01)

s.data_set.filter(variable__name="tmean")


qs = Data.objects.filter(variable__name="tmean", station__tcid='YYJ')
tmt = [ ob.value for ob in qs ]
tmtime = [ ob.time for ob in qs ]

print tmtime,tmt
[ t.year for t in tmtime ]
