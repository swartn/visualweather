from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'visualweather.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^stations/', include('stations.urls', namespace="stations")),
    url(r'^admin/', include(admin.site.urls)),
)
